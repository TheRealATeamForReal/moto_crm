<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Carbon\Carbon;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Complaint::class, function (Faker\Generator $faker) {

    static $issues = [ 'Battery Issue', 'Speaker not working', 'Display not working', 'Touchpad not working', 'Phone is not getting switched on', 'Mic not working', 'Buttons not working', 'Software Issue'];


    static $devices = [ 'Moto G 1st Generation', 'Moto G 2nd Generation', 'Moto G 3rd Generation', 'Moto G Turbo 3rd Generation', 'Moto G Play 4th Generation', 'Moto G Plus 4th Generation', 'Moto G 5th Generation', 'Moto G Plus 5th Generation', 'Moto E 1st Generation', 'Moto E 2nd Generation' ];


    static $location = [ 'Borivali West, Mumbai', 'Ville Parle, Mumbai', 'Dadar East, Mumbai', 'Rastapeth, Pune', 'Koramangala, Bengaluru' ];

    static $warranty = ['Yes', 'No'];
    return [
        'user_id' => User::where('is_admin', 0)->inRandomOrder()->first()->id,
        'device' => $devices[random_int(0, 9)],
        'location' => $location[random_int(0, 4)],
        'issue' => $issues[random_int(0, 7)],
        'description' => $faker->sentence(8),
        'warranty' => $warranty[random_int(0,1)],
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});

$factory->define(App\Feedback::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'mobile' => $faker->phoneNumber,
        'subject' => $faker->sentence(5),
        'message' => $faker->sentence(15),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});