<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Yash Gill',
            'email' => 'yash@moto.com',
            'password' => bcrypt('secret'),
            'is_admin' => 1
        ]);
        User::create([
            'name' => 'Manthan Kansara',
            'email' => 'manthan@moto.com',
            'password' => bcrypt('secret'),
            'is_admin' => 1
        ]);
        factory(App\User::class, 200)->create();
    }
}
