<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel='shortcut icon' type='image/x-icon' href='/images/moto-favicon.ico' />

    <!-- Styles -->
    <link href="/css/all.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <style>
        .userView {
            background-image: url('/images/background.png');
        }
        .side-nav-active {
            background-color: #169CEE;
            color:white;
        }
        .side-nav a {
            color:inherit ;
            display: block;
            font-size: 14px;
            font-weight: 500;
            height: 48px;
            line-height: 48px;
            padding: 0 32px;
        }

        .removeMargin {
            margin: 0
        }
        #user_name {
            padding-bottom: 10px;
        }
    </style>

    @yield('head')
</head>
<body>
<div id="app">

    <div class="navbar-fixed white">
        <div class="nav-wrapper">
            <ul>
                <li class="black-text">Home</li>
            </ul>
            <ul id="slide-out" class="side-nav fixed">
                <li>
                    <div class="userView">
                        <a href="#!user"><img class="circle" src="/images/user1.ico"></a>
                        <a href="#!name"><h3 class="flow-text white-text removeMargin">{{ Auth::user()->name }}</h3></a>
                        <a href="#!email"><h5 class="flow-text white-text removeMargin" id="user_name">{{ Auth::user()->email }}</h5></a>
                    </div>
                </li>
                <div class="divider"></div>
                <li id="side_nav_dashboard"><a href="/dashboard" class="waves-effect side_nav_item"><i class="material-icons black-text">dashboard</i>Dashboard</a></li>
                <li id="side_nav_report"><a href="/report" class="waves-effect side_nav_item"><i class="material-icons black-text">&#xE868;</i>Complaints Report</a></li>
                <li id="side_nav_analysis"><a href="/analysis" class="waves-effect side_nav_item"><i class="material-icons black-text">&#xE8B5;</i>Analysis</a></li>
                <li id="side_nav_feedback"><a href="/feedback" class="waves-effect side_nav_item"><i class="material-icons black-text">&#xE87F;</i>Feedback</a></li>
                <li>
                    <a class="menu-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="waves-effect side_nav_item">
                        <i class="material-icons black-text">exit_to_app</i>Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons black-text">menu</i></a>
        </div>
    </div>

    @yield('content')
</div>

<!-- Scripts -->
<script src="/js/all.js"></script>

<script>
    $(document).ready(() => {
        $(".button-collapse").sideNav();
        $('select').material_select();

        let url = window.location.pathname.slice(1);
        $('#side_nav_' + url).addClass('side-nav-active');
    });
</script>
@yield('scripts')

</body>
</html>
