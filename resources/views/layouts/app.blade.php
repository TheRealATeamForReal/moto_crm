<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel='shortcut icon' type='image/x-icon' href='/images/moto-favicon.ico' />

    <!-- Styles -->
    <link href="css/all.css" rel="stylesheet">
    <link href="css/sweetalert2.css">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    @yield('head')
</head>
<body>
    <div id="app">

        <div class="navbar-fixed">
            <nav class="white">
                <div class="nav-wrapper">
                    <a href="{{url('welcome')}}" class="brand-logo"><img src = "images/logo-animation-header-sm-0005.png" width="90%" height="90%"></a>
                    <a href="#" data-activates="mobile-nav" class="button-collapse"><i class="material-icons black-text">&#xE5D2;</i></a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        @if (Auth::guest())
                            <li><a class="menu-item" href="/login">Login</a></li>
                            <li><a class="menu-item" href="/register">Register</a></li>
                        @else
                            @if (Auth::user()->is_admin == 1)
                                <li><a class="menu-item" href="/dashboard">Dashboard</a></li>
                            @else
                                <li><a class="menu-item" href="/complaint">Service and Repairs</a></li>
                                <li><a class="menu-item" href="/drivers">Drivers Update</a></li>
                                <li><a class="menu-item" href="/contact">Contact Us</a></li>
                            @endif
                            <li>
                                <a class="menu-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                </form>
                            </li>
                        @endif
                </div>
            </nav>
        </div>
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/all.js"></script>

    <script>
        $(document).ready(() => {
            $(".button-collapse").sideNav();
            $('select').material_select();
        });
    </script>
    @yield('scripts')

</body>
</html>
