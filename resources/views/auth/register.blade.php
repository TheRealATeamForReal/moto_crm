@extends('layouts.app')

@section('head')
    <style>
        .center_align {
            display: block;
            margin: 0 auto;
        }
        .moto-blue {
            color: #367cdf;
        }
        .moto-blue-background {
            background-color: #367cdf;
        }
        #signupBtn:hover {
            background-color: #367cdf;
        }
        body {
            background: -webkit-linear-gradient(to left, #8e9eab , #eef2f3); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #8e9eab , #eef2f3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        }
    </style>
@endsection
@section('content')
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Register</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                            {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Register--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row valign-wrapper">
    <div class="col l4 m12 s12 center_align valign" id="cardCol">
        <div class="loginCard card white hoverable z-depth-4" >
            <div class="card-content " id="loginContent">
                <img class="responsive-img center_align" src="/images/logo-animation-header-sm-0005.png" />
                <h2 id="loginText" class="center-align moto-blue">Register</h2>

                <div class="row" id="registerFormDiv">

                    <form name="registerForm" id="registerForm" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col l12 m12 s12 black-text">
                                <i class="material-icons prefix blue-text">person</i>
                                <input name="name" type="text" id="name" class="validate" minlength="3" maxlength="32" required>
                                <label for="name" data-error="Invalid name">Name</label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col l12 m12 s12 black-text ">
                                <i class="material-icons prefix blue-text">mail_outline</i>
                                <input id="email" type="email" name="email" class="validate" maxlength="64" required>
                                <label for="email" data-error="Invalid Email Id">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col l12 m12 s12 black-text ">
                                <i class="material-icons prefix blue-text ">lock</i>
                                <input id="password" type="password" name="password" class="validate" minlength="6" required>
                                <label for="password" data-error="Invalid Password">Password</label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col l12 m12 s12 black-text ">
                                <i class="material-icons prefix blue-text ">lock</i>
                                <input id="password-confirm" type="password" name="password_confirmation" class="validate" minlength="6" required>
                                <label for="password-confirm" data-error="Invalid Password">Confirm Password</label>

                            </div>
                        </div>
                        <div class="row center" id="submitBtnRow">
                            <div class="col l12 m12 s12">
                                <button id="signupBtn" class="btn waves-effect waves-light btn-large moto-blue-background hoverable" type="submit" name="submit">Get Started</button>
                            </div>
                        </div>
                        <div class="col s12 l12 m12">
                            <div id="loginDiv" class="card-content center preLink grey-text text-darken-2">Already a member?<a id="loginLink" href="/login">Login</a></div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

</div>
@endsection
