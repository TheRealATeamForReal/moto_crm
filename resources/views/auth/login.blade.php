@extends('layouts.app')

@section('head')
    <style>
        .center_align {
            display: block;
            margin: 0 auto;
        }
        .moto-blue {
            color: #367cdf;
        }
        .moto-blue-background {
            background-color: #367cdf;
        }
        body {
            background: -webkit-linear-gradient(to left, #8e9eab , #eef2f3); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #8e9eab , #eef2f3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        }
    </style>
@endsection
@section('content')
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Login</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                        {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-8 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Login--}}
                                {{--</button>--}}

                                {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                    {{--Forgot Your Password?--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="row valign-wrapper">
    <div class="col l4 m12 s12 center_align valign" id="cardCol">
        <div class="loginCard card white hoverable" >
            <div class="card-content " id="loginContent">
                <img class="responsive-img center_align" src="/images/logo-animation-header-sm-0005.png" />
                <h2 id="loginText" class="center-align moto-blue">Login</h2>

                <div class="row" id="loginFormDiv">

                    <form name="loginForm" id="loginForm" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col l12 m12 s12 black-text field{{ $errors->has('email') ? ' error' : '' }}">
                                <i class="material-icons prefix blue-text">email</i>
                                <input name="email" type="email" id="email" class="validate" value="{{ old('email') }}" required autofocus>
                                <label data-error="Invalid Email ID" for="email">Email</label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col l12 m12 s12 black-text field{{ $errors->has('password') ? ' error' : '' }}">
                                <i class="material-icons prefix blue-text">security</i>
                                <input name="password" type="password" id="password" class="validate " placeholder=" " autocomplete="new-password">
                                <label data-error="Wrong Password" for="password">Password</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col l12 m12 s12">
                                <div class="left">
                                    <input type="checkbox" id="remember" name="remember" class="" />
                                    <label class="black-text" for="remember">Remember me?</label>
                                </div>
                                <div class="right">
                                    <a class="moto-blue right" id="forgot_pswd_link" href="">Forgot Your Password?</a>
                                </div>

                            </div>
                        </div>

                        <div class="row center">
                            <div class="col l12 m12 s12" id="loginSubmitBtnRow">

                                <button class="btn waves-effect waves-light moto-blue-background" id="loginSubmitBtn" type="submit" name="submit">Login</button>

                            </div>
                        </div>
                        @if(count($errors->all()) > 0)
                            <div class="red-text">
                                @foreach($errors->all() as $error)
                                        <p class="center-align">{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                    </form>

                </div>

            </div>
        </div>
    </div>

</div>
@endsection
