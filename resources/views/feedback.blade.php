@extends('layouts.master')

@section('head')
    <title>Feedback</title>
    <style>

    </style>
@endsection

@section('content')
    <br />
    <br />

    <div class="row">

        <div class="col l9 m8 s12 offset-l3 offset-l2 card white hoverable">
            <h1 class="flow-text teal-text">Feedback</h1>

            <br />
            <table id="complaints_table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Subject</th>
                    <th>Message</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        const app = new Vue({
            el: '#app',

            created() {

            },

            data: {

            },

            methods: {

            }

        });

        $(document).ready(() => {
            $('#complaints_table').DataTable({
                responsive: true,
                columnDefs: [
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ],

                ajax: '/api/feedback',

                columns: [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "email" },
                    { "data": "mobile" },
                    { "data": "subject" },
                    { "data": "message" },
                ]
            });
        });
    </script>
@endsection