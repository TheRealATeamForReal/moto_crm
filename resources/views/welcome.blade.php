<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Moto CRM</title>

        <link rel="stylesheet" href="/css/all.css">
        <link rel="stylesheet" href="/vegas/vegas.min.css">

        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
        </script>

    </head>
    <body>
        {{--<div class="flex-center position-ref full-height">--}}
            {{--@if (Route::has('login'))--}}
                {{--<div class="top-right links">--}}
                    {{--@if (Auth::check())--}}
                        {{--<a href="{{ url('/home') }}">Home</a>--}}
                    {{--@else--}}
                        {{--<a href="{{ url('/login') }}">Login</a>--}}
                        {{--<a href="{{ url('/register') }}">Register</a>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--@endif--}}

            {{--<div class="content">--}}
                {{--<div class="title m-b-md">--}}
                    {{--Laravel--}}
                {{--</div>--}}

                {{--<div class="links">--}}
                    {{--<a href="https://laravel.com/docs">Documentation</a>--}}
                    {{--<a href="https://laracasts.com">Laracasts</a>--}}
                    {{--<a href="https://laravel-news.com">News</a>--}}
                    {{--<a href="https://forge.laravel.com">Forge</a>--}}
                    {{--<a href="https://github.com/laravel/laravel">GitHub</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}


        {{--<a class="waves-effect waves-light btn">button</a>--}}
        {{--<a class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>button</a>--}}
        {{--<a class="waves-effect waves-light btn"><i class="material-icons right">cloud</i>button</a>--}}
        <div class="navbar-fixed">
            <nav class="white">
                <div class="nav-wrapper">
                    <a href="{{url('welcome')}}" class="brand-logo"><img src = "images/logo-animation-header-sm-0005.png" width="90%" height="90%"></a>
                    <a href="#" data-activates="mobile-nav" class="button-collapse"><i class="material-icons black-text">&#xE5D2;</i></a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li class="menu-item"><a href="{{url('complaint')}}">Service and Repairs</a></li>
                        <li class="menu-item"><a  href="{{url('drivers')}}">Drivers Update</a></li>
                        <li class="menu-item"><a href="{{url('contact')}}">Contact Us</a></li>
                        <li class="menu-item"><a href="{{url('login')}}">Login</a></li>
                        <li class="menu-item"><a href="{{url('register')}}">Register</a></li>
                    </ul>
                    <ul id="mobile-nav" class="side-nav">
                        <li><a class="black-text" href="{{url('complaint')}}">Service and Repairs</a></li>
                        <li><a class="black-text" href="{{url('drivers')}}">Drivers Update</a></li>
                        <li><a class="black-text" href="{{url('contact')}}">Contact Us</a></li>
                        <li><a class="black-text" href="{{url('login')}}">Login</a></li>
                        <li><a class="black-text" href="{{url('register')}}">Register</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="slider col l12 m12 s12">
            <a href="https://www.motorola.in/products/smartphones" id="learn-more-btn" class="waves-effect waves-light btn btn-large blue hoverable">Learn More</a>
        </div>

    </body>

    <script src="/js/all.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
    <script src="/vegas/vegas.min.js"></script>

    <script>
        $(document).ready (function () {
            $(".button-collapse").sideNav();
        });
        $(".slider").vegas({
            cover:true,
            autoplay:true,
            loop: true,
            slides: [
                { src: "/images/mot-direct-homepage-bf-1-d-eu.svg" },
                { src: "/images/mot-direct-homepage-bf-2-d-eu.svg" },
                { src: "/images/mot-direct-homepage-bf-4-d-eu.svg" },
                { src: "/images/mot-homepage-hero-motog5-family-d.svg" },
                { src: "/images/moto-moto-m-homepage-hero-d-row.svg" }
            ]
        });
    </script>
</html>
