@extends('layouts.master')

@section('head')
    <title>Dashboard</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:700" rel="stylesheet">

    <style>
        .stats {
            font-family: 'Roboto Slab', 'Roboto', serif;
            font-size: 2.4em;
            height: 130px;
            text-align: center;
            background: #169cee;
            color: white;
        }
    </style>
@endsection

@section('content')
    <div class="row">

        <div class="col l4 m4 s6 offset-l3 offset-l2 card teal hoverable stats">
            <br/>
            <br/>

            <div class="progress" v-if="! complaints">
                <div class="indeterminate"></div>
            </div>
            <span v-text="complaints" v-else></span>

            <br/>
            <br/>

            Complaints
            <br/>

        </div>
        <div class="col l4 m4 s6 offset-l1 offset-l1 card blue hoverable stats">
            <br/>
            <br/>

            <div class="progress" v-if="! feedback">
                <div class="indeterminate"></div>
            </div>
            <span v-text="feedback" v-else></span>

            <br/>
            <br/>

            Feedback
            <br/>

        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <div class="col l4 m4 s6 offset-l3 offset-l2 card cyan hoverable stats">
            <br/>
            <br/>

            <div class="progress" v-if="! users">
                <div class="indeterminate"></div>
            </div>
            <span v-text="users" v-else></span>

            <br/>
            <br/>

            Users
            <br/>

        </div>
        <div class="col l4 m4 s6 offset-l1 offset-l1 card grey hoverable stats">
            <br/>
            <br/>

            <div class="progress" v-if="! admins">
                <div class="indeterminate"></div>
            </div>
            <span v-text="admins" v-else></span>

            <br/>
            <br/>

            Admins
            <br/>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        const app = new Vue({
            el: '#app',

            created() {
                function getComplaints() {
                    return window.axios.get('/api/complaints_count');
                }

                function getFeedback() {
                    return window.axios.get('/api/feedback_count');
                }

                function getUsers() {
                    return window.axios.get('/api/users');
                }

                function getAdmins() {
                    return window.axios.get('/api/admins');
                }

                let vm = this;
                window.axios.all([getComplaints(), getFeedback(), getUsers(), getAdmins()])
                    .then(window.axios.spread(function (complaints, feedback, users, admins) {
                        vm.complaints = complaints.data;
                        vm.feedback = feedback.data;
                        vm.users = users.data;
                        vm.admins = admins.data;
                    }));
            },

            data: {
                complaints: 0,
                feedback: 0,
                users: 0,
                admins: 0
            },

            methods: {

            }

        });
    </script>
@endsection