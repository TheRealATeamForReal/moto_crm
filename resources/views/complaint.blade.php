@extends('layouts.app')

@section('head')
    <title>Service and Repairs</title>
    <style>
        .slider {
            height:90vh;
        }
        body {
            background: -webkit-linear-gradient(to left, #8e9eab , #eef2f3); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #8e9eab , #eef2f3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        }
        #motocare-logo {
            margin-top:5%;
            margin-left:1%;
        }
        #motocare-tagline {
            margin-top:3%;
            font-family: "Open Sans";
        }
        #service_row {
            margin-top:5%;
        }
        #formHeader {
            margin-top:5%;
        }
        .moto-light-blue-background {
            background-color: #00bbd3;
        }

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="card col l10 m8 s12 offset-l1 offset-m2">
            <div id="motocare-logo" class="row">
                <div id="" class="col l4">
                    <img src="/images/logo-motocare.png">
                </div>
                <div class="col l8">
                    <h5 id="motocare-tagline">We offer coverage throughout India for service and repairs.</h5>
                </div>
            </div>
            <div class="row" id="service_row">

                <div id="serviceCol" class="col l8 offset-l2 card hoverable">
                    <h5 id="formHeader" class="header center-align flow-text">Submit your complaint</h5>
                    <p class="center-align" style="margin-top:5%;">Please enter the details about your phone and complaint to provide you with the services.</p>
                    <div class="row">
                        <div class="divider" style="margin-top:5%; margin-bottom:2%;"></div>

                        <form class="col l12" id="complaint_form" name="complaintForm" @submit.prevent="submitForm">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="input-field col l12 m12 s12">
                                    {{--<select class="icons" name="model-name" id="model-name" v-model="form.device">--}}
                                            {{--<option :data-icon="device.image" v-for="device in devices" :value="device.text" v-text="device.text"></option>--}}
                                            <p class="flow-text">Select your Device</p>
                                            <select class="icons browser-default" name="model-name" id="model-name" v-model="form.device">
                                                <option data-icon="/images/motog.jpg" value="Moto G 1st Generation">Moto G 1st Generation</option>
                                                <option value="Moto G 2nd Generation">Moto G 2nd Generation</option>
                                                <option value="Moto G 3rd Generation">Moto G 3rd Generation</option>
                                                <option value="Moto G Turbo 3rd Generation">Moto G Turbo 3rd Generation</option>
                                                <option value="Moto G Play 4th Generation">Moto G Play 4th Generation</option>
                                                <option value="Moto G Plus 4th Generation">Moto G Plus 4th Generation</option>
                                                <option value="Moto G 5th Generation">Moto G 5th Generation</option>
                                                <option value="Moto G Plus 5th Generation">Moto G Plus 5th Generation</option>
                                                <option value="Moto E 1st Generation">Moto E 1st Generation</option>
                                                <option value="Moto E 2nd Generation">Moto E 2nd Generation</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col l12 m12 s12">
                                    <p class="flow-text">Select your Preferred Service Center</p>
                                    <select class="browser-default" name="location" id="location" v-model="form.location">
                                        <option value="Borivali West, Mumbai">Borivali West, Mumbai</option>
                                        <option value="Ville Parle, Mumbai">Ville Parle, Mumbai</option>
                                        <option value="Dadar East, Mumbai">Dadar East, Mumbai</option>
                                        <option value="Rastapeth, Pune">Rastapeth, Pune</option>
                                        <option value="Koramangala, Bengaluru">Koramangala, Bengaluru</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col l12 m12 s12">
                                    <p>What's wrong with your phone?</p>
                                    <input type="checkbox" id="battery-complaint" value="Battery Issue" v-model="form.issue">
                                    <label class="black-text" for="battery-complaint">Battery Issue</label><br>
                                    <input type="checkbox" id="speaker-complaint" value="Speaker not working" v-model="form.issue">
                                    <label class="black-text" for="speaker-complaint">Speaker not working</label><br>
                                    <input type="checkbox" id="display-complaint" value="Display not working" v-model="form.issue">
                                    <label class="black-text" for="display-complaint">Display not working</label><br>
                                    <input type="checkbox" id="touchpad-complaint" value="Touchpad not working" v-model="form.issue">
                                    <label class="black-text" for="touchpad-complaint">Touchpad not working</label><br>
                                    <input type="checkbox" id="dead-complaint" value="Phone is not getting switched on" v-model="form.issue">
                                    <label class="black-text" for="dead-complaint">Phone is not getting switched on</label><br>
                                    <input type="checkbox" id="mic-complaint" value="Mic not working" v-model="form.issue">
                                    <label class="black-text" for="mic-complaint">Mic not working</label><br>
                                    <input type="checkbox" id="button-complaint" value="Buttons not working" v-model="form.issue">
                                    <label class="black-text" for="button-complaint">Buttons not working</label><br>
                                    <input type="checkbox" id="software-complaint" value="Software Issue" v-model="form.issue">
                                    <label class="black-text" for="software-complaint">Software Issue</label><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col l12 m12 s12">
                                    <textarea id="complaint-box" class="materialize-textarea" v-model="form.description"></textarea>
                                    <label for="complaint-box" class="black-text">Please give detailed information about your complaint</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col l12 m12 s12">
                                    <p class="black-text">Is your mobile phone still in waranty period?</p>
                                    <input type="radio" id="warrantyYes" name="warranty" value="Yes" v-model="form.warranty">
                                    <label for="warrantyYes" class="black-text">Yes</label>
                                    <input type="radio" id="warrantyNo" name="warranty" value="No" v-model="form.warranty">
                                    <label for="warrantyNo" class="black-text">No</label>
                                </div>
                            </div>
                            <div class="divider" style="margin-top:5%;"></div>
                            <div class="row right" style="margin-top: 6%;">
                                <button class="waves-effect waves-light moto-light-blue-background btn-large hoverable" type="submit">Submit your complaint<i class="material-icons right">&#xE163;</i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        const app = new Vue({
            el: '#service_row',

            created() {
//                $('select').material_select();
            },

            data: {
//                devices: [
//                    {
//                        text: 'Moto G 1st Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto G 2nd Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto G 3rd Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto G Turbo 3rd Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto G Play 4th Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto G Plus 4th Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto G 5th Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto G Plus 5th Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto E 1st Generation',
//                        image: ''
//                    },
//                    {
//                        text: 'Moto E 2nd Generation',
//                        image: ''
//                    }
//                ],
                form: {
                    device: 'Moto G 1st Generation',
                    location: 'Borivali West, Mumbai',
                    issue: [],
                    description: '',
                    warranty: ''
                }
            },

            methods: {
                submitForm() {
                    window.axios.post('/complaint', this.form)
                        .then((response) => {
                            if(response.data.success) {
                                swal({
                                    title: "Success",
                                    text: "Your complaint has been registered successfully. Our representative will contact you shortly.",
                                    type: "success",
                                    timer: 60000
                                });
                            }
                        })
                        .catch((response) => {

                        });
                }
            }

        });
    </script>
@endsection