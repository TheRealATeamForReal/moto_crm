@extends('layouts.master')

@section('head')
    <title>Reports</title>
    <style>

    </style>
@endsection

@section('content')
    <br />
    <br />

    <div class="row">

        <div class="col l9 m8 s12 offset-l3 offset-l2 card white hoverable">
            <h1 class=" teal-text">Analysis</h1>

            <chartjs-radar :width="800" :height="600" :datalabel="'Issues'" :labels="issuesLabels" :data="issues" :bind="true"></chartjs-radar>

            <br />
            <hr>
            <br />

            <chartjs-bar :width="900" :height="500" :datalabel="'Devices'" :labels="devicesLabels" :data="devices" :bind="true"></chartjs-bar>

            <br />
            <hr>
            <br />

            <p class="flow-text">Warranty</p>
            <chartjs-doughnut :width="800" :height="300" :datalabel="'Warranty'" :labels="['Yes', 'No']" :data="warranties"  :backgroundcolor="['rgba(75,192,192,1)', 'rgba(244,67,54,1)']"  :hoverbackgroundcolor="[ 'rgba(33,150,243,1)', 'rgba(198,40,40,1)',]" :bind="true"></chartjs-doughnut>

            <br />
            <hr>
            <br />

            <p class="flow-text">Locations</p>
            <chartjs-polar-area :datalabel="'Locations'" :labels="locationsLabels" :data="locations" :bind="true" :width="600" :height="400"></chartjs-polar-area>
            {{--<chartjs-polar-area :datalabel="'TestDataLabel'" :labels="['happy','myhappy','hello']" :data="[100,40,60]"></chartjs-polar-area>--}}

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        Vue.use(VueCharts);
        const app = new Vue({
            el: '#app',

            created() {
                function getIssues() {
                    return window.axios.get('/api/issues');
                }

                function getDevices() {
                    return window.axios.get('/api/devices');
                }

                function getWarranties() {
                    return window.axios.get('/api/warranties');
                }

                function getLocations() {
                    return window.axios.get('/api/locations');
                }

                let vm = this;
                window.axios.all([getIssues(), getDevices(), getWarranties(), getLocations()])
                    .then(window.axios.spread(function (issues, devices, warranties, locations) {
                        vm.issues = issues.data;
                        vm.devices = devices.data;
                        vm.warranties = warranties.data;
                        vm.locations = locations.data;
                    }));
            },

            data: {
                issuesLabels: ['Battery', 'Speaker','Display', 'Touchpad', 'Phone', 'Mic', 'Buttons', 'Software'],
                issues: [],

                devicesLabels: [ 'Moto G 1st Generation', 'Moto G 2nd Generation', 'Moto G 3rd Generation', 'Moto G Turbo 3rd Generation', 'Moto G Play 4th Generation', 'Moto G Plus 4th Generation', 'Moto G 5th Generation', 'Moto G Plus 5th Generation', 'Moto E 1st Generation', 'Moto E 2nd Generation' ],
                devices: [],

                warranties: [],

                locationsLabels: [ 'Borivali West, Mumbai', 'Ville Parle, Mumbai', 'Dadar East, Mumbai', 'Rastapeth, Pune', 'Koramangala, Bengaluru' ],
                locations: []
            },
            methods: {

            }

        });
    </script>
@endsection