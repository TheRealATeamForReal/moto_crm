@extends('layouts.app')

@section('head')
    <title>Contact Us</title>
    <style>
        body {
            background: -webkit-linear-gradient(to left, #8e9eab , #eef2f3); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #8e9eab , #eef2f3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        }
        .margin-top {
            margin-top:2%;
        }
        .center_align {
            display: block;
            margin: 0 auto;
        }
        .moto-purple-color {
            background-color: #762751;
        }
        #submit:hover {
            background-color: #762751;
        }
    </style>
@endsection

@section('content')
    <div class="row" id="contact_row">
        <div class="row removeMarginTopBottom">
            <div id="contact-section" class="container">
                <h3 class="center flow-text">Fill in the form and we will get back to you in no time.</h3>
            </div>
        </div>

        <div class="row removeMarginTopBottom">
            <div id="contact-form" class="card col s10 m8 l6 offset-l3 offset-m2 offset-s1 hoverable z-depth-4">
                <div class="row margin-top">
                    <div class="col l2 m2 s2">
                        <img src="/images/moto-logo-purple.png" width="50%" height="50%">
                    </div>
                    <div class="col l8 m8 s8">
                        <h5 class="center-align">Contact Us</h5>
                    </div>
                </div>
                <form name="contactForm" action="" id="contactForm" @submit.prevent="submitForm">

                    <div class="row removeMarginTopBottom">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix grey-text">&#xE853;</i>
                            <input id="full_name" type="text" name="name" class="validate" minlength="3" maxlength="32" required v-model="form.name">
                            <label for="full_name">Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix grey-text">&#xE0BE;</i>
                            <input id="email" type="text" name="email" class="validate" maxlength="64" required v-model="form.email">
                            <label for="email">Email Address</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix grey-text">&#xE0CD;</i>
                            <input id="mobile" type="number" name="mobile" class="validate" minlength="10" maxlength="10" required v-model="form.mobile">
                            <label for="mobile">Mobile Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix grey-text">&#xE8D2;</i>
                            <input id="subject" type="text" name="subject" class="validate" maxlength="64" required v-model="form.subject">
                            <label for="subject">Subject</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix grey-text">&#xE0C9;</i>
                            <textarea id="message" name="message" class="materialize-textarea" length="2000" required class="validate" v-model="form.message"></textarea>
                            <label for="message">Message</label>
                        </div>
                    </div>
                    <div class="row">
                        <button class="btn waves-effect waves-light right btn-large teal hoverable" type="submit" name="action" id="submit">Submit<span><i class="material-icons white-text right">&#xE163;</i></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        const app = new Vue({
            el: '#contact_row',

            data: {
                form: {
                    name: '',
                    email: '',
                    mobile: '',
                    subject: '',
                    message: ''
                }
            },

            methods: {
                submitForm() {
                    window.axios.post('/contact', this.form)
                        .then((response) => {
                            if(response.data.success) {
                                swal({
                                    title: "Success",
                                    text: "Your feedback has been saved. We will get back to you shortly!",
                                    type: "success",
                                    timer: 60000
                                });
                            }
                        })
                        .catch((response) => {

                        });
                }
            }

        });
    </script>

@endsection