@extends('layouts.app')

@section('head')
    <title>Drivers</title>
    <link rel='shortcut icon' type='image/x-icon' href='/images/moto-favicon.ico' />
    <style>
        body {
            background: -webkit-linear-gradient(to left, #8e9eab , #eef2f3); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #8e9eab , #eef2f3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        }
        .bordered {
            border:1px solid;
        }
        .moto-blue {
            color: #367cdf;
        }
        .moto-red-background {
            background-color: #d54030;
        }
        .margin-top {
            margin-top:2%;
        }
        .download-btn:hover {
            background-color: #d54030;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="card col l10 m8 s12 offset-l1 offset-m2">
            <div class="row margin-top">
                <div class="col l2 m2 s2">
                    <img src="images/moto-logo-red.png" height="50%" width="50%">
                </div>
                <div class="col l4 s4 m4 offset-l2 offset-s2 offset-m2">
                    <h4 class="center-align header">Drivers Update</h4>
                </div>
            </div>
            <div class="row">
                <p class="center-align">For certain Android-powered devices, Motorola Device Manager can also update your software. <a href="http://motorola-mobility-en-in.custhelp.com/app/answers/detail/a_id/91506" class="moto-blue">Check out how.</a></p>
            </div>
            <div class="row">
                <h5>System Requirements:</h5>
            </div>
            <div class="row">
                <div class="col l5 m12 s12 offset-l1 bordered">
                    <h5 class="header">Windows®</h5>
                    <ul>
                        <li>Windows XP® (SP3 or greater)</li>
                        <li>Windows Vista®</li>
                        <li>Windows 7®</li>
                        <li>Windows 8®</li>
                        <li>Windows 10®</li>
                    </ul>
                    <p>Works with devices running Android®, Motorola OS, or Windows Mobile® operating systems</p>
                    <div class="row center">
                        <a href="http://www.motorola.com/getmdmwin" class="btn waves-effect waves-light download-btn moto-red-background hoverable">Download for Windows<i class="material-icons left">&#xE2C4;</i></a>
                    </div>
                </div>
                <div class="col l5 m12 s12 bordered">
                    <h5 class="header" style="padding-bottom:5%">Mac OS X®</h5>
                    <ul>
                        <li>Mac OS® 10.5.8 Leopard</li>
                        <li>Mac OS® 10.6 Snow Leopard</li>
                        <li>Mac OS® 10.7 Lion</li>
                        <li>Mac OS® 10.8 Mountain Lion®</li>
                        <li>Mac OS® 10.10 Yosemite</li>
                    </ul>
                    <p>Works with devices running Android®</p>
                    <div class="row center">
                        <a href="http://www.motorola.com/getmdmmac" class="btn waves-effect waves-light hoverable download-btn  moto-red-background">Download for Mac<i class="material-icons left">&#xE2C4;</i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection