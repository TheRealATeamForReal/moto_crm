@extends('layouts.master')

@section('head')
    <title>Reports</title>
    <style>

    </style>
@endsection

@section('content')
    <br />
    <br />

    <div class="row">

        <div class="col l9 m8 s12 offset-l3 offset-l2 card white hoverable">
            <h1 class="flow-text teal-text">Complaints</h1>

            <br />
            <table id="complaints_table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Device</th>
                    <th>Location</th>
                    <th>Issue</th>
                    <th>Description</th>
                    <th>Warranty</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        const app = new Vue({
            el: '#app',

            created() {

            },

            data: {

            },

            methods: {

            }

        });

        $(document).ready(() => {
            $('#complaints_table').DataTable({
                responsive: true,
                columnDefs: [
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ],

                ajax: '/api/complaints',

                columns: [
                    { "data": "id" },
                    { "data": "user.name" },
                    { "data": "device" },
                    { "data": "location" },
                    { "data": "issue" },
                    { "data": "description" },
                    { "data": "warranty" }
                ]
            });
        });
    </script>
@endsection