<?php

namespace App\Notifications;

use App\Complaint;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ComplaintRegistered extends Notification implements ShouldQueue
{
    use Queueable;
    /**
     * @var Complaint
     */
    public $complaint;

    /**
     * Create a new notification instance.
     *
     * @param Complaint $complaint
     */
    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hi ' .  $this->complaint->user->name . '!')
                    ->line('Your complaint has been registered successfully!')
                    ->line('The complaint no. is ' . $this->complaint->id)
                    ->line('Device: ' . $this->complaint->device)
                    ->line('Location: ' . $this->complaint->location)
                    ->line('Issue: ' . $this->complaint->issue)
                    ->line('Description: ' . $this->complaint->description)
                    ->line('Warranty: ' . $this->complaint->warranty)
                    ->line('We will contact you very shortly to resolve all your issues by sending a representative at your place.')
                    ->action('Get Driver Support', url('/drivers'))
                    ->line('Thank you for using our application!');
    }
}
