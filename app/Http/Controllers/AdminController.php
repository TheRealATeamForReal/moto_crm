<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function showDashboard()
    {
        return view('dashboard');
    }

    public function showAnalysis()
    {
        return view('analysis');
    }

    public function showReport()
    {
        return view('report');
    }

    public function showFeedback()
    {
        return view('feedback');
    }
}
