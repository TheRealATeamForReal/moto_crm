<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $table = "complaints";

    protected $fillable = [
        'user_id',
        'device',
        'location',
        'issue',
        'description',
        'warranty'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
