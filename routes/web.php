<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Complaint;
use App\Feedback;
use App\Notifications\ComplaintRegistered;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@showComplaint');

Route::get('/complaint', 'HomeController@showComplaint');

Route::get('/dashboard', 'AdminController@showDashboard');
Route::get('/report', 'AdminController@showReport');
Route::get('/analysis', 'AdminController@showAnalysis');
Route::get('/feedback', 'AdminController@showFeedback');


Route::get('/drivers', function() {
    return view('drivers');
});

Route::get('/contact', function() {
    return view('contact');
});

Route::post('/complaint', function(Request $request) {
    $data = $request->all();
    $user =  auth()->user();

    $data['issue'] = implode($data['issue'], ', ');
    $data['user_id'] = $user->id;
    $complaint = Complaint::create($data);

    $user->notify(new ComplaintRegistered($complaint));
    return Response::json([
        'success' => true,
        'message' => 'Complaint registered successfully!'
    ]);
});

Route::post('/contact', function(Request $request) {
    $data = $request->all();

    Feedback::create($data);

    return Response::json([
        'success' => true,
        'message' => 'Feedback saved successfully!'
    ]);
});