<?php
use App\Complaint;
use App\Feedback;
use App\Notifications\ComplaintRegistered;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/complaints', function() {
    $complaints = Complaint::all();
    foreach($complaints as $complaint) {
        $complaint->user;
    }
    $complaints = $complaints->toArray();
    return Response::json([
        'data' => $complaints
    ]);
});

Route::get('/feedback', function() {
    $feedbacks = Feedback::all()->toArray();
    return Response::json([
        'data' => $feedbacks
    ]);
});


Route::get('/issues', function() {
    $fields = ['Battery', 'Speaker','Display', 'Touchpad', 'Phone', 'Mic', 'Buttons', 'Software'];

    foreach($fields as $field) {
        $issues[] = count(Complaint::where('issue', 'like', '%' . $field . '%')->get());
    }

    return $issues;
});

Route::get('/devices', function() {
    $fields = [ 'Moto G 1st Generation', 'Moto G 2nd Generation', 'Moto G 3rd Generation', 'Moto G Turbo 3rd Generation', 'Moto G Play 4th Generation', 'Moto G Plus 4th Generation', 'Moto G 5th Generation', 'Moto G Plus 5th Generation', 'Moto E 1st Generation', 'Moto E 2nd Generation' ];

    foreach($fields as $field) {
        $devices[] = count(Complaint::where('device', $field)->get());
    }

    return $devices;
});

Route::get('/warranties', function() {
    $fields = [ 'Yes', 'No' ];

    foreach($fields as $field) {
        $warranty[] = count(Complaint::where('warranty', $field)->get());
    }

    return $warranty;
});

Route::get('/locations', function() {
    $fields = [ 'Borivali West, Mumbai', 'Ville Parle, Mumbai', 'Dadar East, Mumbai', 'Rastapeth, Pune', 'Koramangala, Bengaluru' ];

    foreach($fields as $field) {
        $locations[] = count(Complaint::where('location', $field)->get());
    }

    return $locations;
});

Route::get('/complaints_count', function() {
    $complaints = count(Complaint::all());
    return Response::json([
        $complaints
    ]);
});

Route::get('/feedback_count', function() {
    $feedback = count(Feedback::all());
    return Response::json([
        $feedback
    ]);
});

Route::get('/users', function() {
    $users = count(User::where('is_admin',0)->get());
    return Response::json([
        $users
    ]);
});

Route::get('/admins', function() {
    $admins = count(User::where('is_admin', 1)->get());
    return Response::json([
        $admins
    ]);
});